#pragma once

#ifndef _GUI_H_
#define _GUI_H_

#define DIRECTINPUT_VERSION 0x0800

#include <windows.h>
#include <d3dx9tex.h>
#include <iostream>
#include <string>

#include "imgui/imgui.h"
#include "imgui/imgui_impl_dx9.h"
#include "imgui/imgui_impl_win32.h"
#include "config.h"

class c_gui {
public:
	static void show();

private:
	static void apply_theme();

	static void update_window_pos(HWND win_handle, UINT msg, WPARAM win_param, LPARAM long_param);
	static LRESULT		__stdcall window_proc(HWND win_handle, UINT msg, WPARAM win_param, LPARAM long_param);

	static LPCSTR		window_class_name;
	static WNDCLASSEX	window_class;
	static HWND			window_handle;
	static MSG			message;

	static LPDIRECT3DDEVICE9		g_pd3dDevice;
	static D3DPRESENT_PARAMETERS	g_d3dpp;

	static int x_click;
	static int y_click;
};

#endif