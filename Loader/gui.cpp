#include "gui.h"

#define DIRECTINPUT_VERSION 0x0800

LPDIRECT3DDEVICE9		c_gui::g_pd3dDevice = nullptr;
D3DPRESENT_PARAMETERS	c_gui::g_d3dpp;

LPCSTR		c_gui::window_class_name = "squirr3l001";
WNDCLASSEX	c_gui::window_class;
HWND		c_gui::window_handle;
MSG			c_gui::message;

int	c_gui::x_click;
int	c_gui::y_click;

struct settings {
public:
	std::string username = "";
	std::string password = "";
	std::string hwid	 = "";

	bool loggedin	= false;
	bool minimized  = false;
	bool shown		= true;
};

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND win_handle, UINT msg, WPARAM win_param, LPARAM long_param);
LRESULT __stdcall c_gui::window_proc(HWND win_handle, UINT msg, WPARAM win_param, LPARAM long_param) {
	if (ImGui_ImplWin32_WndProcHandler(win_handle, msg, win_param, long_param)) return 1;

	switch (msg) {
	case WM_LBUTTONDOWN:
		SetCapture(win_handle);

		x_click = LOWORD(long_param);
		y_click = HIWORD(long_param);
		break;

	case WM_LBUTTONUP:
		ReleaseCapture();
		break;

	case WM_MOUSEMOVE:
		update_window_pos(win_handle, msg, win_param, long_param);
		break;

	case WM_SIZE:
		if (g_pd3dDevice != nullptr && win_param != SIZE_MINIMIZED) {
			ImGui_ImplDX9_InvalidateDeviceObjects();
			g_d3dpp.BackBufferWidth = LOWORD(long_param);
			g_d3dpp.BackBufferHeight = HIWORD(long_param);

			if (g_pd3dDevice->Reset(&g_d3dpp) == D3DERR_INVALIDCALL) IM_ASSERT(0);

			ImGui_ImplDX9_CreateDeviceObjects();
		}

		break;

	case WM_CLOSE:
		DestroyWindow(win_handle);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(win_handle, msg, win_param, long_param);
	}

	return 0;
}

void c_gui::apply_theme() {
	ImGuiStyle& style = ImGui::GetStyle();

	style.AntiAliasedLines = true;

	style.FramePadding = ImVec2(4, 2);
	style.ItemSpacing = ImVec2(6, 2);
	style.ItemInnerSpacing = ImVec2(6, 4);
	style.WindowRounding = 3.0f;
	style.WindowTitleAlign = ImVec2(0.5, 0.5);
	style.FrameRounding = 3.0f;
	style.IndentSpacing = 6.0f;
	style.ItemInnerSpacing = ImVec2(2, 4);
	style.ColumnsMinSpacing = 50.0f;
	style.GrabMinSize = 14.0f;
	style.GrabRounding = 16.0f;
	style.ScrollbarSize = 12.0f;
	style.ScrollbarRounding = 16.0f;

	style.Colors[ImGuiCol_Text] = ImVec4(0.90f, 0.90f, 0.90f, 1.00f);
	style.Colors[ImGuiCol_TextDisabled] = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
	style.Colors[ImGuiCol_WindowBg] = ImVec4(0.16f, 0.16f, 0.16f, 1.0f);
	style.Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.16f, 0.16f, 0.16f, 1.0f);
	style.Colors[ImGuiCol_PopupBg] = ImVec4(0.16f, 0.2f, 0.24f, 1.0f);
	style.Colors[ImGuiCol_Border] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	style.Colors[ImGuiCol_FrameBg] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.35f, 0.62f, 0.99f, 1.0f);
	style.Colors[ImGuiCol_FrameBgActive] = ImVec4(0.35f, 0.62f, 0.99f, 1.0f);
	style.Colors[ImGuiCol_TitleBg] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.08f, 0.51f, 0.10f, 0.51f);
	style.Colors[ImGuiCol_TitleBgActive] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_MenuBarBg] = ImVec4(0.40f, 0.40f, 0.55f, 0.80f);
	style.Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.08f, 0.39f, 0.10f, 0.39f);
	style.Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.26f, 0.56f, 0.96f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.26f, 0.56f, 0.96f, 0.74f);
	style.Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.26f, 0.56f, 0.96f, 0.90f);
	style.Colors[ImGuiCol_CheckMark] = ImVec4(0.90f, 0.90f, 0.90f, 1.00f);
	style.Colors[ImGuiCol_SliderGrab] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_Button] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_ButtonHovered] = ImVec4(0.35f, 0.62f, 0.99f, 1.0f);
	style.Colors[ImGuiCol_ButtonActive] = ImVec4(0.35f, 0.62f, 0.99f, 1.0f);
	style.Colors[ImGuiCol_Header] = ImVec4(0.0f, 0.25f, 1.0f, 1.0f);
	style.Colors[ImGuiCol_HeaderHovered] = ImVec4(0.0f, 0.43f, 1.0f, 1.0f);
	style.Colors[ImGuiCol_HeaderActive] = ImVec4(0.0f, 0.25f, 1.0f, 1.0f);
	style.Colors[ImGuiCol_Column] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_ColumnHovered] = ImVec4(0.35f, 0.62f, 0.99f, 1.0f);
	style.Colors[ImGuiCol_ColumnActive] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_ResizeGrip] = ImVec4(0.08f, 0.27f, 0.10f, 0.30f);
	style.Colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.35f, 0.62f, 0.99f, 1.0f);
	style.Colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);
	style.Colors[ImGuiCol_PlotLines] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	style.Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.00f, 0.00f, 1.00f, 0.35f);
	style.Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.20f, 0.20f, 0.20f, 0.35f);
}

static bool clicked = false;

void c_gui::update_window_pos(HWND win_handle, UINT msg, WPARAM win_param, LPARAM long_param) {
	RECT window;
	RECT desktop;

	const POINTS points = MAKEPOINTS(long_param);

	const int xMouse = points.x;
	const int yMouse = points.y;

	if (yMouse < 18 && GetCapture() == win_handle && !clicked) clicked = true;

	if (clicked) {
		GetWindowRect(win_handle, &window);
		GetWindowRect(GetDesktopWindow(), &desktop);

		const int xWindow = window.left + xMouse - x_click;
		const int yWindow = window.top + yMouse - y_click;

		SetWindowPos(win_handle, HWND_TOPMOST, xWindow, yWindow, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
	}

	if (GetCapture() != win_handle) clicked = false;
}

void c_gui::show() {
	window_class.cbSize = sizeof(WNDCLASSEX);
	window_class.style = CS_HREDRAW | CS_VREDRAW;
	window_class.lpfnWndProc = window_proc;
	window_class.cbClsExtra = 0;
	window_class.cbWndExtra = 0;
	window_class.hInstance = GetModuleHandle(nullptr);
	window_class.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	window_class.hCursor = LoadCursor(nullptr, IDC_ARROW);
	window_class.hbrBackground = HBRUSH(COLOR_WINDOW + 1);
	window_class.lpszMenuName = nullptr;
	window_class.lpszClassName = window_class_name;
	window_class.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);

	if (!RegisterClassEx(&window_class)) {
		MessageBox(nullptr, ("Window Registration Failed!"), ("Error!"),
			MB_ICONEXCLAMATION | MB_OK);
		exit(0);
	}

	window_handle = CreateWindowEx(
		SS_SUNKEN,
		window_class_name,
		"",
		WS_POPUP | WS_VISIBLE | WS_SYSMENU,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		WINDOW_WIDTH, WINDOW_HEIGHT,
		nullptr, nullptr,
		window_class.hInstance,
		nullptr
	);

	if (window_handle == nullptr) {
		MessageBoxA(nullptr, "window creation failed", "error", 0);
		exit(0);
	}

	LPDIRECT3D9 pD3D = Direct3DCreate9(D3D_SDK_VERSION);

	if (pD3D == nullptr) {
		UnregisterClass(TEXT(window_class_name), window_class.hInstance);
		exit(0);
	}

	ZeroMemory(&g_d3dpp, sizeof(g_d3dpp));

	g_d3dpp.Windowed = TRUE;
	g_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	g_d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	g_d3dpp.EnableAutoDepthStencil = TRUE;
	g_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	g_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

	if (pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, window_handle, D3DCREATE_HARDWARE_VERTEXPROCESSING, &g_d3dpp, &g_pd3dDevice) < 0) {
		pD3D->Release();
		UnregisterClass(TEXT(window_class_name), window_class.hInstance);
		MessageBoxA(nullptr, "failed to setup d3d device", "error", 0);
		exit(0);
	}

	IMGUI_CHECKVERSION();

	ImGui::CreateContext();
	ImGui_ImplWin32_Init(window_handle);
	ImGui_ImplDX9_Init(g_pd3dDevice);

	apply_theme();

	ZeroMemory(&message, sizeof(message));

	settings settings;

	char username_buffer[100];
	char password_buffer[100];

	LPDIRECT3DTEXTURE9 pTexture = nullptr;
	D3DXCreateTextureFromFileInMemory(g_pd3dDevice, image_data, sizeof(image_data), &pTexture);

	while (message.message != WM_QUIT) {
		if (PeekMessage(&message, nullptr, 0U, 0U, PM_REMOVE)) {
			TranslateMessage(&message);
			DispatchMessage(&message);
			continue;
		}

		ImGui_ImplDX9_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();

		const auto dwFlag = ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse;

		if (ImGui::Begin(LOADER_NAME, &settings.shown, ImVec2(WINDOW_WIDTH, WINDOW_HEIGHT), 1.0f, dwFlag)) {
			ImGui::Indent((WINDOW_WIDTH - (WINDOW_WIDTH / 1.15)) / 3);

			ImGui::Spacing();
			ImGui::Spacing();
			ImGui::Spacing();

			ImGui::Image(pTexture, ImVec2(WINDOW_WIDTH / 1.17, 150));

			ImGui::Spacing();
			ImGui::Spacing();

			if (settings.loggedin) {

			}
			else {
				if (ImGui::InputText(" ",  username_buffer, sizeof(username_buffer))) { settings.username = username_buffer; }
				if (ImGui::InputText("  ", password_buffer, sizeof(username_buffer))) { settings.password = password_buffer; }
			}
		}

		ImGui::End();
		ImGui::EndFrame();

		g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, false);
		g_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
		g_pd3dDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, false);

		const ImVec4 clear_color = ImVec4(0.26f, 0.56f, 0.96f, 1.0f);

		D3DCOLOR clear_col_dx = D3DCOLOR_RGBA(
			static_cast<int>(clear_color.x),
			static_cast<int>(clear_color.y),
			static_cast<int>(clear_color.z),
			static_cast<int>(clear_color.w)
		);
		
		g_pd3dDevice->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, clear_col_dx, 1.0f, 0);

		if (g_pd3dDevice->BeginScene() >= 0) {
			ImGui::Render();
			ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
			g_pd3dDevice->EndScene();
		}

		// Handle loss of D3D9 device
		if (g_pd3dDevice->Present(nullptr, nullptr, nullptr, nullptr) == D3DERR_DEVICELOST && g_pd3dDevice->TestCooperativeLevel() == D3DERR_DEVICENOTRESET) {
			ImGui_ImplDX9_InvalidateDeviceObjects();
			g_pd3dDevice->Reset(&g_d3dpp);
			ImGui_ImplDX9_CreateDeviceObjects();
		}

		if (!settings.shown) {
			break;
		}
	}

	ImGui_ImplDX9_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	if (g_pd3dDevice) g_pd3dDevice->Release();
	if (pD3D) pD3D->Release();
	DestroyWindow(window_handle);
	UnregisterClass(TEXT(window_class_name), window_class.hInstance);
}