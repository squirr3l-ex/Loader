#include "injection.h"

uintptr_t c_injection::window_pid(const std::string& window_name) {
	unsigned long pid = 0;
	GetWindowThreadProcessId(FindWindow(nullptr, window_name.c_str()), &pid);

	if (!OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid))
		return 0;

	return pid;
}

bool c_injection::inject(std::string& buffer, const uintptr_t& pid) {
	HANDLE hThread, hToken;
	PVOID image, mem;
	DWORD ExitCode;

	TOKEN_PRIVILEGES tp;
	MANUAL_INJECT ManualInject;

	if (OpenProcessToken(HANDLE(-1), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) {
		tp.PrivilegeCount           = 1;
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		tp.Privileges[0].Luid.LowPart  = 20;
		tp.Privileges[0].Luid.HighPart = 0;

		AdjustTokenPrivileges(hToken, FALSE, &tp, 0, nullptr, nullptr);
		CloseHandle(hToken);
	}

	if (!VirtualAlloc(nullptr, buffer.size(), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE)) {
		return false;
	}

	char header_data[248];

	for (int i         = 0; i < 248; i++) {
		header_data[i] = buffer[i];
		buffer[i]      = '0';
	}

	const auto image_data = LPVOID(buffer.data());

	const auto p_idh = PIMAGE_DOS_HEADER(header_data);

	if (p_idh->e_magic != IMAGE_DOS_SIGNATURE) {
		VirtualFree(image_data, 0, MEM_RELEASE);
		return false;
	}

	auto p_inh = PIMAGE_NT_HEADERS(LPBYTE(image_data) + p_idh->e_lfanew);

	if (p_inh->Signature != IMAGE_NT_SIGNATURE) {
		VirtualFree(image_data, 0, MEM_RELEASE);
		return false;
	}

	if (!(p_inh->FileHeader.Characteristics & IMAGE_FILE_DLL)) {
		VirtualFree(image_data, 0, MEM_RELEASE);
		return false;
	}

	const auto h_process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);

	if (!h_process) {
		VirtualFree(image_data, 0, MEM_RELEASE);
		CloseHandle(h_process);

		return false;
	}

	image = VirtualAllocEx(h_process, nullptr, p_inh->OptionalHeader.SizeOfImage, MEM_COMMIT | MEM_RESERVE,
	                       PAGE_EXECUTE_READWRITE);

	if (!image) {
		VirtualFree(image_data, 0, MEM_RELEASE);
		CloseHandle(h_process);

		return false;
	}

	if (!WriteProcessMemory(h_process, image, image_data, p_inh->OptionalHeader.SizeOfHeaders, nullptr)) {
		VirtualFreeEx(h_process, image, 0, MEM_RELEASE);
		CloseHandle(h_process);

		VirtualFree(image_data, 0, MEM_RELEASE);
		return false;
	}

	const auto image_section_header = PIMAGE_SECTION_HEADER(p_inh + 1);

	for (int i = 0; i < p_inh->FileHeader.NumberOfSections; i++) {
		WriteProcessMemory(h_process, PVOID(LPBYTE(image) + image_section_header[i].VirtualAddress),
		                   PVOID(LPBYTE(image_data) + image_section_header[i].PointerToRawData),
		                   image_section_header[i].SizeOfRawData, nullptr);
	}

	mem = VirtualAllocEx(h_process, nullptr, 4096, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

	if (!mem) {
		VirtualFreeEx(h_process, image, 0, MEM_RELEASE);
		CloseHandle(h_process);

		VirtualFree(image_data, 0, MEM_RELEASE);
		return false;
	}

	memset(&ManualInject, 0, sizeof(MANUAL_INJECT));

	ManualInject.ImageBase      = image;
	ManualInject.NtHeaders      = PIMAGE_NT_HEADERS(LPBYTE(image) + p_idh->e_lfanew);
	ManualInject.BaseRelocation = PIMAGE_BASE_RELOCATION(p_inh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress + LPBYTE(image));
	ManualInject.ImportDirectory = PIMAGE_IMPORT_DESCRIPTOR(p_inh->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress + LPBYTE(image));
	ManualInject.fnLoadLibraryA   = LoadLibraryA;
	ManualInject.fnGetProcAddress = GetProcAddress;

	WriteProcessMemory(h_process, mem, &ManualInject, sizeof(MANUAL_INJECT), nullptr);
	WriteProcessMemory(h_process, PVOID(PMANUAL_INJECT(mem) + 1), load_dll, 0 - uintptr_t(load_dll), nullptr);

	hThread = CreateRemoteThread(h_process, nullptr, 0, LPTHREAD_START_ROUTINE(PMANUAL_INJECT(mem) + 1), mem, 0,
	                             nullptr);

	if (!hThread) {
		VirtualFreeEx(h_process, mem, 0, MEM_RELEASE);
		VirtualFreeEx(h_process, image, 0, MEM_RELEASE);
		VirtualFree(image_data, 0, MEM_RELEASE);

		CloseHandle(h_process);

		return false;
	}

	WaitForSingleObject(hThread, INFINITE);
	GetExitCodeThread(hThread, &ExitCode);

	if (!ExitCode) {
		VirtualFreeEx(h_process, mem, 0, MEM_RELEASE);
		VirtualFreeEx(h_process, image, 0, MEM_RELEASE);
		VirtualFree(image_data, 0, MEM_RELEASE);

		CloseHandle(hThread);
		CloseHandle(h_process);

		return false;
	}

	VirtualFreeEx(h_process, mem, 0, MEM_RELEASE);

	CloseHandle(hThread);
	CloseHandle(h_process);

	if (!p_inh->OptionalHeader.AddressOfEntryPoint) {
		return false;
	}

	VirtualFree(image_data, 0, MEM_RELEASE);

	return true;
}

DWORD c_injection::load_dll(void* p) {
	uintptr_t Function;
	uintptr_t* ptr;
	PIMAGE_THUNK_DATA FirstThunk, OrigFirstThunk;

	const auto ManualInject = PMANUAL_INJECT(p);
	const auto delta        = reinterpret_cast<uintptr_t>(LPBYTE(ManualInject->ImageBase) - ManualInject
	                                                                                        ->NtHeaders->OptionalHeader.
	                                                                                        ImageBase);

	auto p_ibr = ManualInject->BaseRelocation;

	while (p_ibr->VirtualAddress) {
		if (p_ibr->SizeOfBlock >= sizeof(IMAGE_BASE_RELOCATION)) {
			const int count      = (p_ibr->SizeOfBlock - sizeof(IMAGE_BASE_RELOCATION)) / sizeof(WORD);
			const uint16_t* list = reinterpret_cast<uint16_t*>(p_ibr + 1);

			for (auto i = 0; i < count; i++) {
				if (list[i]) {
					ptr = reinterpret_cast<uintptr_t*>(LPBYTE(ManualInject->ImageBase) + (p_ibr->VirtualAddress + (list[
						i] & 0xFFF)));
					*ptr += delta;
				}
			}
		}

		p_ibr = PIMAGE_BASE_RELOCATION(LPBYTE(p_ibr) + p_ibr->SizeOfBlock);
	}

	auto p_iid = ManualInject->ImportDirectory;

	// Resolve DLL imports

	while (p_iid->Characteristics) {
		OrigFirstThunk = PIMAGE_THUNK_DATA(LPBYTE(ManualInject->ImageBase) + p_iid->OriginalFirstThunk);
		FirstThunk     = PIMAGE_THUNK_DATA(LPBYTE(ManualInject->ImageBase) + p_iid->FirstThunk);

		const auto h_module = ManualInject->fnLoadLibraryA(LPCSTR(ManualInject->ImageBase) + p_iid->Name);

		if (!h_module) {
			return FALSE;
		}

		while (OrigFirstThunk->u1.AddressOfData) {
			if (OrigFirstThunk->u1.Ordinal & IMAGE_ORDINAL_FLAG) {
				// Import by ordinal

				Function = uintptr_t(
					ManualInject->fnGetProcAddress(h_module, LPCSTR(OrigFirstThunk->u1.Ordinal & 0xFFFF)));

				if (!Function) {
					return FALSE;
				}
				FirstThunk->u1.Function = Function;
			}

			else {
				// Import by name

				const auto image_import_by_name = PIMAGE_IMPORT_BY_NAME(
					LPBYTE(ManualInject->ImageBase) + OrigFirstThunk->u1.AddressOfData);
				Function = uintptr_t(ManualInject->fnGetProcAddress(h_module, LPCSTR(image_import_by_name->Name)));

				if (!Function) {
					return FALSE;
				}

				FirstThunk->u1.Function = Function;
			}

			OrigFirstThunk++;
			FirstThunk++;
		}

		p_iid++;
	}

	if (ManualInject->NtHeaders->OptionalHeader.AddressOfEntryPoint) {
		const auto EntryPoint = PDLL_MAIN(
			LPBYTE(ManualInject->ImageBase) + ManualInject->NtHeaders->OptionalHeader.AddressOfEntryPoint);
		return EntryPoint(HMODULE(ManualInject->ImageBase), DLL_PROCESS_ATTACH, nullptr); // Call the entry point
	}

	return TRUE;
}
