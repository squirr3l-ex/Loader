#pragma once

#ifndef _H_CRYPTO_
#define _H_CRYPTO_

#include <iostream>

#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/filters.h>

class c_crypto {
public:
	static std::string base64_encode(const std::string &input);
	static std::string base64_decode(const std::string &input);

	static std::string aes_encrypt(const std::string &input, unsigned char key[16], unsigned char iv[16]);
	static std::string aes_decrypt(const std::string &input, unsigned char key[16], unsigned char iv[16]);

	static unsigned char* random_bytes(int amount = 16);

private:
};

#endif