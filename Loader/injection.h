#pragma once

#ifndef _H_INJECTION_
#define _H_INJECTION_

#include <windows.h>
#include <iostream>

class c_injection {
public:
	static bool inject(std::string &buffer, const uintptr_t &pid);
	static uintptr_t window_pid(const std::string &windowName);

private:
	static DWORD load_dll(void *p);

	typedef HMODULE(WINAPI *pLoadLibraryA)(LPCSTR);
	typedef FARPROC(WINAPI *pGetProcAddress)(HMODULE, LPCSTR);

	typedef BOOL(WINAPI *PDLL_MAIN)(HMODULE, DWORD, PVOID);

	typedef struct _MANUAL_INJECT {
		LPVOID ImageBase;
		PIMAGE_NT_HEADERS NtHeaders;
		PIMAGE_BASE_RELOCATION BaseRelocation;
		PIMAGE_IMPORT_DESCRIPTOR ImportDirectory;
		pLoadLibraryA fnLoadLibraryA;
		pGetProcAddress fnGetProcAddress;
	} MANUAL_INJECT, *PMANUAL_INJECT;
};

#endif