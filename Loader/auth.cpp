#include "auth.h"

size_t write_data(void* ptr, size_t size, size_t nmemb, void* stream);
size_t write_to_string(void* ptr, size_t size, size_t count, void* stream);

std::string c_auth::login(const std::string& username, const std::string& password) {
	std::string post_data;

	post_data += "username=" + username;
	post_data += "&password=" + password;
	post_data += "&hwid=" + hwid();
	post_data += "&method=login";

	return sendPost(std::string(WEB_FOLDER AUTH_FILE), post_data);
}

std::string c_auth::download_dll(const std::string& username, const std::string& password) {
	std::string post_data;

	post_data += "username=" + username;
	post_data += "&password=" + password;
	post_data += "&hwid=" + hwid();
	post_data += "&method=inject";

	return sendPost(WEB_FOLDER AUTH_FILE, post_data);
}

std::string c_auth::hwid() {
	return "hwid nerd";
}

std::string c_auth::sendPost(std::string url, std::string& data) {
	CURL* curl = curl_easy_init();

	std::string response_string;

	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
	curl_easy_setopt(curl, CURLOPT_PROXY, nullptr);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);

	if (curl_easy_perform(curl) != CURLE_OK) {
		curl_easy_cleanup(curl);

		return R"({"status":"failed", "description":"connection error"})";
	}

	curl_easy_cleanup(curl);

	return response_string;
}

std::string c_auth::downloadData(const std::string& url) {
	CURL* curl = curl_easy_init();
	std::stringstream out;

	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "deflate");
	curl_easy_setopt(curl, CURLOPT_PROXY, nullptr);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &out);

	if (curl_easy_perform(curl) != CURLE_OK) {
		curl_easy_cleanup(curl);
		exit(0);
	}

	curl_easy_cleanup(curl);

	return out.str();
}

size_t write_data(void* ptr, size_t size, size_t nmemb, void* stream) {
	const std::string data(static_cast<const char*>(ptr), static_cast<size_t>(size) * nmemb);
	*static_cast<std::stringstream*>(stream) << data << std::endl;
	return size * nmemb;
}

size_t write_to_string(void* ptr, size_t size, size_t count, void* stream) {
	static_cast<std::string*>(stream)->append(static_cast<char*>(ptr), 0, size * count);
	return size * count;
}
