#include "crypto.h"

std::string c_crypto::base64_encode(const std::string& input) {
	std::string result;
	CryptoPP::StringSource(input, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(result)));
	return result;
}

std::string c_crypto::base64_decode(const std::string& input) {
	std::string result;
	CryptoPP::StringSource(input, true, new CryptoPP::Base64Decoder(new CryptoPP::StringSink(result)));

	return result;
}

std::string c_crypto::aes_encrypt(const std::string& input, unsigned char key[16], unsigned char iv[16]) {
	// storage
	std::string encryptedData;

	// create cypher text
	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(encryptedData));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(input.c_str()), input.length());
	stfEncryptor.MessageEnd();

	return encryptedData;
}

std::string c_crypto::aes_decrypt(const std::string& input, unsigned char key[16], unsigned char iv[16]) {
	// storage
	std::string decryptedData;

	// create deciphered text
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedData));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(input.c_str()), input.size());
	stfDecryptor.MessageEnd();

	return decryptedData;
}

unsigned char* c_crypto::random_bytes(int amount) {
	const auto result = new unsigned char[amount];

	CryptoPP::AutoSeededRandomPool rng;
	rng.GenerateBlock(result, amount);

	return result;
}
