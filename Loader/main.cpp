#include <windows.h>

#include "auth.h"
#include "crypto.h"
#include "injection.h"
#include "json.h"
#include "gui.h"

#include "config.h"

using json = nlohmann::json;

void refresh() {
	system("CLS");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN);

	std::cout << "   _____       __    ___           _             __" << std::endl;
	std::cout << "  / ___/__  __/ /_  / (_)___ ___  (_)___  ____ _/ /" << std::endl;
	std::cout << "  \\__ \\/ / / / __ \\/ / / __ `__ \\/ / __ \\/ __ `/ / " << std::endl;
	std::cout << " ___/ / /_/ / /_/ / / / / / / / / / / / / /_/ / /  " << std::endl;
	std::cout << "/____/\\__,_/_.___/_/_/_/ /_/ /_/_/_/ /_/\\__,_/_/   " << std::endl;
	std::cout << "                                                   " << std::endl;
}

void log(const std::string& message, bool endl = true) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN);
	std::cout << (std::string(LOG_PREFIX) + " " + message).c_str();
	if (endl) std::cout << std::endl;
}

void error(const std::string& message, bool endl = true) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED);
	std::cout << (std::string(LOG_PREFIX) + " " + message).c_str();
	if (endl) std::cout << std::endl;
}

int main() {
	SetConsoleTitleA("Squirr3l");
	refresh();

	std::string username;
	std::string password;

	json responsedata;

	for (;;) {
		log("username: ", false);
		std::getline(std::cin, username);

		log("password: ", false);
		std::getline(std::cin, password);

		responsedata = json::parse(c_auth::login(username, password));

		if (responsedata["status"].get<std::string>() != "success") {
			error(responsedata["description"].get<std::string>());
			continue;
		}

		break;
	}

	refresh();

	log("welcome: " + responsedata["username"].get<std::string>());
	log("injecting!");

	auto key = responsedata["key"].get<std::string>();
	auto iv  = responsedata["iv"].get<std::string>();

	auto buffer = c_crypto::aes_decrypt(
		c_crypto::base64_decode(c_auth::download_dll(username, password)),
		(unsigned char *)c_crypto::base64_decode(key).c_str(),
		(unsigned char *)c_crypto::base64_decode(iv).c_str()
	);

	if (!c_injection::inject(buffer, c_injection::window_pid("steam"))) {
		log("fail");
	}

	log("injected!");

	getchar();
}
