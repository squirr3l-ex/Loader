#pragma once

#ifndef _H_CONFIG_
#define _H_CONFIG_

#define LOADER_NAME		"squirrel"
#define LOG_PREFIX		"[$]"

#define WINDOW_WIDTH	250
#define WINDOW_HEIGHT	350

#define WEB_HEADER	"squirr3l"
#define WEB_FOLDER	"http://localhost/test/"
#define AUTH_FILE	"auth.php"

const unsigned char image_data[] = {
	0x0, 0x0
};

#endif