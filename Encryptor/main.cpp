#include <iostream>
#include <fstream>
#include <iomanip>
#include <direct.h>

#include <cryptopp/osrng.h>
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/filters.h>

#include <windows.h>

unsigned char* random_bytes(int amount) {
	const auto result = new unsigned char[amount];

	CryptoPP::AutoSeededRandomPool rng;
	rng.GenerateBlock(result, amount);

	return result;
}

std::string base64_encode(const std::string& in) {

	std::string out;

	int val = 0, valb = -6;
	for (unsigned char c : in) {
		val = (val << 8) + c;
		valb += 8;
		while (valb >= 0) {
			out.push_back("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[(val >> valb) & 0x3F]);
			valb -= 6;
		}
	}
	if (valb > -6)
		out.push_back(
			"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[((val << 8) >> (valb + 8)) & 0x3F]);
	while (out.size() % 4) out.push_back('=');

	return out;
}

std::string base64_decode(const std::string& in) {
	std::string out;

	std::vector<int> T(256, -1);
	for (int i = 0; i < 64; i++) T["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i]] = i;

	int val = 0, valb = -8;
	for (unsigned char c : in) {
		if (T[c] == -1) break;
		val = (val << 6) + T[c];
		valb += 6;
		if (valb >= 0) {
			out.push_back(char((val >> valb) & 0xFF));
			valb -= 8;
		}
	}

	return out;
}

std::string fileExtension(const std::string& filename) {
	size_t i = filename.rfind('.', filename.length());

	if (i == std::string::npos) {
		return "";
	}

	return filename.substr(filename.find_last_of('.') + 1);
}

std::string fileName(const std::string& filepath) {
	size_t i = filepath.rfind('\\', filepath.length());

	if (i == std::string::npos) {
		return "";
	}

	return filepath.substr(filepath.find_last_of('\\') + 1);
}

const std::string readAllBytes(const std::string& filename) {

	const auto fileHandle = CreateFile(filename.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, 0, nullptr);
	const auto dwFileSize = GetFileSize(fileHandle, nullptr);

	auto* p = static_cast<char *>(malloc(dwFileSize));

	DWORD dwBytesRead;
	ReadFile(fileHandle, p, dwFileSize, &dwBytesRead, nullptr);

	std::string fileContents;
	fileContents.assign(p, dwFileSize);
	free(p);

	return fileContents;
}

std::string encryptFile(std::string buffer, unsigned char key[16], unsigned char iv[16]) {
	// storage
	std::string encryptedData;

	// create cypher text
	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(encryptedData));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(buffer.c_str()), buffer.length());
	stfEncryptor.MessageEnd();

	return encryptedData;
}

std::string decryptFile(std::string buffer, unsigned char key[16], unsigned char iv[16]) {
	// storage
	std::string decryptedData;

	// create deciphered text
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedData));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(buffer.c_str()), buffer.size());
	stfDecryptor.MessageEnd();

	return decryptedData;
}

int main(int argc, char** argv) {
	SetConsoleTitle("Encryptor");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE);

	if (argc == 1) {
		std::cout << "[~]: drag and drop file onto me to encrypt!" << std::endl;
		std::cout << "[~]: closing!" << std::endl;
	}

	for (int i            = 1; i < argc; i++) {
		const auto buffer = readAllBytes(argv[i]);
		const auto key    = random_bytes(16);
		const auto iv     = random_bytes(16);

		const std::string enc_buffer = base64_encode(encryptFile(buffer, key, iv));
		const std::string dec_buffer = decryptFile(base64_decode(enc_buffer), key, iv);

		if (buffer == dec_buffer) {
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
			std::cout << "Encryption successful" << std::endl;
		}
		else {
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED);
			std::cout << "Encryption failed!" << std::endl;
		}

		_mkdir("Encrypted");

		const auto ext      = fileExtension(argv[i]);
		const auto filename = fileName(argv[i]);

		const auto path = "Encrypted\\" + filename.substr(0, filename.length() - ext.length() - 1) + ".enc";

		std::fstream fs;

		fs.open(path, std::fstream::out);
		fs << enc_buffer;

		fs.close();

		fs.open("Encrypted\\" + filename.substr(0, filename.length() - ext.length() - 1) + "-key.txt",
		        std::fstream::out);
		fs << std::string("[--[RESULTS]--]") << std::endl;
		fs << std::string("key: " + base64_encode(std::string(reinterpret_cast<const char*>(key)))) << std::endl;
		fs << std::string("iv: " + base64_encode(std::string(reinterpret_cast<const char*>(iv)))) << std::endl;
		fs << std::string("[-------------]");

		fs.close();
	}

	getchar();
}
