#pragma once

#ifndef _H_AUTH_
#define _H_AUTH_

#include <iostream>

#include <curl/curl.h>
#include <curl/easy.h>
#include <sstream>

#include "config.h"

class c_auth {
public:
	static std::string login(const std::string &username, const std::string &password);
	static std::string download_dll(const std::string &username, const std::string &password);

private:
	static std::string hwid();
	static std::string sendPost(std::string url, std::string &data);
	static std::string downloadData(const std::string &url);
};

#endif